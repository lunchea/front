'use strict'

###*
 # @ngdoc directive
 # @name lunchboxApp.directive: All order-related directives
 # @description
 # #  All order-related directives
###


angular.module 'lunchboxApp.orders', [
  'lunchboxApp.auth'
]

.filter "truncate", ()->
  return (input, max)->
    return input.slice(0, max) if input


# Directive for a whole order
.directive "order", () ->
  restrict: "E"
  templateUrl: "/components/orders/order.html"

.directive "steppingStone", ["$timeout", "$rootScope", ($timeout, $rootScope) ->
  restrict: "A"
  link: (scope, element, attrs) ->
    change_height = () ->
      height = $(attrs["target"]).height()
      if scope.order.totalCost() and $rootScope.from != "sbuforum" > 0
        element.animate height: height + 20, 200
      else
        element.animate height: 0, 200
    scope.$watch "order.totalCost()", ()->
      $timeout(change_height, 100)
    scope.$watch "layout.is_show_detail", ()->
      $timeout(change_height, 100)
]


.directive "orderSummary", ["$rootScope", "$timeout", ($rootScope, $timeout) ->
  restrict: "A",
  templateUrl: "/components/orders/panel-order-summary.html",
  link: (scope, element, attrs) ->
    # Record current scroll top into $scope.scroll
    # panel-order-summary has a ng-class="{"low"}" to control the
    # position of the summary panel
    handler = () ->
      scope.scroll = $(window).scrollTop()
    $(window).on("scroll", scope.$apply.bind(scope, handler))
    handler()

    # Monitor GET request para :from
    # Stop watching to save system resource after 10s.
    # if it is from sbuforum, then we must change the css "position"
    # of the panel to "absolute", and dynamically adjust its "top"
    # because the bug of iFrame
    unwatch = $rootScope.$watch "from", ()->
      if $rootScope.from == "sbuforum"
        unwatch()

    # If the page is loaded for a long time (10s), it is not necessary
    # to continue watching the [:from]
    $timeout(()->
      unwatch()
    , 10000)
]

.directive "orderSummaryItem", () ->
  restrict: "A",
  templateUrl: "/components/orders/panel-order-summary-item.html",
  scope: true
  link: (scope, element, attrs) ->
    scope.addative = scope.delivery.categories.indexOf("Sushi") == -1
    # Click each box and scroll to corresponding place in the menu
    scope.scrollTo = (selector) ->
      new_top = $(selector).offset().top - $('.panel-order-summary').height()
      new_top = new_top - 50
      $("html, body").animate scrollTop: new_top
      return

.directive "orderMenu", () ->
  restrict: "E",
  templateUrl: "/components/orders/order-menu.html",
  link: (scope) ->
    scope.setItem = (item) ->
      scope.item = item

.directive "stepInstruction", ()->
  restrict: "E",
  scope:
    step: "@"
    totalSteps: "@"
    instruction: "@"
  templateUrl: "/components/orders/step-instruction.html",

.directive "datePicker", () ->
  restrict: "E",
  templateUrl: "/components/orders/order-date-picker.html",

.directive "orderMenuItem", () ->
  restrict: "E"
  templateUrl: "/components/orders/order-menu-item.html"
  link: (scope, element, attrs)->
    scope.addative = scope.delivery.categories.indexOf("Sushi") == -1


.filter "pref_list", ()->
  return (items)->
    pref_list = []
    for item in items
      for pref in item.preferences
        pref_list.push(pref)
    return pref_list


.directive "preferenceMenuTab", ()->
  restrict: "A"
  link: (scope)->
    scope.layout.display_pref_menu = undefined
    # Set up an ugly watch for the first selectable preference
    # to come up
    # !!! SHOULD REMOVE BY REDESIGNING THE MENU !!!
    unwatch = scope.$watch "delivery",()->
      if scope.delivery.items.length > 0
        item = scope.delivery.items[0]
        if item.preferences?
          if item.preferences[0]?
            pref = item.preferences[0]
            scope.layout.display_pref_menu = pref
            unwatch()
    , true
    scope.switchToMenu = (index)->
      scope.layout.itemIndex = index

    scope.remove_pref = (pref, item, delivery)->
      # console.log "delivery", delivery
      item.preferences.splice item.preferences.indexOf(pref), 1
      delivery.removeItem(item.box)

.directive "preferenceMenu", ()->
  restrict: "E"
  templateUrl: "/components/orders/pref-menu.html"
  scope:
    item: "="

.directive "prefMenuItem", ()->
  restrict: "E"
  templateUrl: "/components/orders/pref-menu-item.html"


# Directive for email-input which contains simple alert
.directive "inputEmail", ['$auth', 'currentUser', ($auth, currentUser) ->
  restrict: "E"
  templateUrl: "/components/orders/input-email.html"
  link: (scope, element, attrs) ->
    scope.auth = $auth
    element.keyup (e) ->
      if e.keyCode is 13
        $("[name='phone']").focus()
      return
    scope.regexEmail = /[\$\%\^\&\\<>\(\)!]+/

    scope.$watch 'auth.isAuthenticated()', (newVal) ->
      if newVal
        scope.email =  currentUser.get().email
        scope.order.email =  currentUser.get().email
        # console.log 'sh', currentUser.get()
        #, _.functions( element.find("input")[0])
      else
        element.value = ""
      return
]

# Directive for phone-number-input which contains simple alert
.directive "inputPhone", ['$auth', 'currentUser', ($auth, currentUser) ->
  restrict: "E"
  templateUrl: "/components/orders/input-phone.html"
  link: (scope, element) ->
    scope.auth = $auth
    scope.regexPhone = /\D+/  # Allow digits only
    # Place order when hitting [Enter] key
    element.keyup (e) ->
      if e.keyCode is 13
        scope.placeOrder()
      return
    scope.$watch 'auth.isAuthenticated()', (newVal) ->
      if newVal
        scope.phone =  currentUser.get().profile.phone
        scope.order.phone = currentUser.get().profile.phone
      else
        element.value = ""
      return
]
