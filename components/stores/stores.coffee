'use strict'

###*
 # @ngdoc directive
 # @name lunchboxApp.directive: All Store-related directives
 # @description
 # #  All Store-related directives
###


angular
.module 'lunchboxApp.stores', [
  'lunchboxApp.common'
]

.directive "storeForm", [
  'Stores'
  (Stores) ->
    restrict: "E"
    scope:
      store: "=ngModel"
    templateUrl: "/components/stores/form.html"
]

.directive "storeFormConfirm", [
  'Stores'
  (Stores) ->
    restrict: "E"
    scope:
      store: "=ngModel"
    templateUrl: "/components/stores/formconfirm.html"
    link: (scope) ->
      if not scope.stores
        scope.stores = $scope.currentUser.get().stores.$resolve()    
]

.directive "storeFormTemplate", [
  'Stores'
  (Stores) ->
    restrict: "E"
    templateUrl: "/components/stores/formtemplate.html"
    link: (scope) ->
      if scope.store
        scope.store.email_at = new Date(scope.store.email_at)
        scope.store.delivered_at = new Date(scope.store.delivered_at)
]