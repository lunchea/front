'use strict'

###*
 # @ngdoc function
 # @name lunchboxApp.controller:UsersCtrl
 # @description
 # # UsersCtrl
 # Controller of the lunchboxApp
###
angular
.module 'lunchboxApp.auth', [
  'lunchboxApp.settings'
  'lunchboxApp.services'
  'ngMaterial'
]

.directive 'login', [
  'User', '$auth', 'currentUser'
  (User, $auth, currentUser) ->
    templateUrl:'/components/auth/login.html'
    restrict: 'E'
    transclude: true
    link: (scope, elem, attrs) ->
      # console.log currentUser.get()
      scope.auth = $auth
      scope.credentials = {}
      scope.login = ()->
        if scope.authForm.$valid
          scope.credentials.alert = "in-progress"
          scope.auth.login(scope.credentials)
          .then (response) ->
            currentUser.init response.data
          , (error) ->
            console.log error
            scope.credentials.alert = error.statusText
        else
          console.log "invalid."
          scope.error_is_show = true
          scope.credentials.alert = "input-error"
]

# Directive for email-input which contains simple alert
.directive "authInputEmail", ['$auth', 'currentUser', ($auth, currentUser) ->
  restrict: "E"
  scope:
    email: '='
  templateUrl: "/components/auth/input-email.html"
  link: (scope, element, attrs) ->
    scope.regexEmail = /[\$\%\^\&\\<>\(\)!]+/
    $("[type='email']").focus()
    return
]

.directive "authInputPwd", ['$auth', 'currentUser', ($auth, currentUser) ->
  restrict: "E"
  templateUrl: "/components/auth/input-pwd.html"
  link: (scope, element) ->
    scope.regexPwd = /[\$\%\^\&\\<>\(\)!]+/
    # element.find("[type='password']").keyup (e) ->
    #   if e.keyCode is 13
    #     scope.login()
    #   return
]
.directive "authInputPwdRepeat", () ->
  restrict: "E"
  templateUrl: "/components/auth/input-pwd-repeat.html"
  link: (scope, element) ->
    scope.regexPwd = /[\$\%\^\&\\<>\(\)!]+/
    # element.find("[type='password']").keyup (e) ->
    #   if e.keyCode is 13
    #     scope.login()
    #   return


.directive 'signUp', ['$auth', ($auth) ->
  templateUrl:'/components/auth/register.html'
  restrict: 'E'
  replace: true
  transclude: true
  controller: [
    '$scope', 'User', '$auth', 'currentUser'
    ($scope, User, $auth, currentUser) ->
      $scope.auth = $auth
      $scope.credentials = {}
      $scope.signUp = ->
        if $scope.credentials.password isnt $scope.credentials.passwordRepeat
          $scope.credentials.alert = "pwds-not-match"
          console.log "do not match"
          return
        if $scope.authForm.$valid
          $scope.auth.signup $scope.credentials
          .then (response) ->
            currentUser.init response.data
          , (error) ->
            console.log error
            $scope.credentials.alert = error.statusText
        else
          console.log "invalid"
          $scope.credentials.alert = "input-error"
  ]
]
.directive 'signOut', [
  '$auth', '$localStorage'
  ($auth, $localStorage) ->
    link: (scope, elem) ->
      elem.bind 'click', () ->
        $auth.logout()
        currentUser.init {}
]

.directive 'sendResetEmail', [
  '$http', 'settings'
  ($http, settings) ->
    templateUrl:'/components/auth/send_reset_email.html'
    restrict: 'E'
    replace: true
    link: (scope) ->
      scope.sent = false
      scope.sendEmail = () ->
        $http.get(settings.apiUrl + '/auth/reset?email='+ scope.email)
        .then (data) ->
          scope.$emit 'Instructions for password reset has been sent.'
          scope.sent = true
        , (error) ->
          scope.$emit 'Email not found.'
          console.log error
]

.directive 'resetPassword', [
  '$http', 'settings'
  ($http, settings) ->
    templateUrl:'/components/auth/reset.html'
    restrict: 'E'
    replace: true
    scope:
      resetPasswordToken: "="
      afterSave: "&"
    link: (scope) ->
      scope.credentials =
        reset_password_token: scope.resetPasswordToken

      scope.reset = () ->
        $http.put(settings.apiUrl + '/auth/reset', scope.credentials)
        .then (data) ->
          scope.$emit 'Password is reset.'
          scope.afterSave()
        , (error) ->
          scope.$emit 'Token invalid.'
          console.log error
]

.directive 'confirmEmail', () ->
  templateUrl:'components/auth/login.html'
  restrict: 'E'
  transclude: true
  scope: {
    token: "=confirmToken"
  }
  controller: [
    '$scope', '$mdToast', '$auth', '$http', 'settings'
    ($scope, $mdToast,  $auth, $http, settings) ->
      $scope.auth = $auth
      # console.log "confirming email"
      if $scope.token
        $http(
          method: 'PUT'
          url: settings.apiUrl + '/auth/confirm.json',
          data:
            confirmation_token: $scope.token
        ).success (data, status, headers) ->
          $mdToast.show(
            $mdToast.simple()
            .content 'Email confirmed succesfully'
          )
          # if (!$localStorage.currentUser)
          # if there is no user logged in yet, login
          #   if currentUser = _decode headers()['x-jwt']
          #     _authenticate currentUser, headers()['x-jwt']
          #   else # Email confirmed, but can't decode here
          #     console.log 'Something went wrong'
        .error (data, status, headers, config) ->
          $mdToast.show(
            $mdToast.simple()
            .content "Can not confirm your email: Invalid token"
          )
  ]

.directive 'dcMatch', -> #used for checking if passwords match
  restrict: 'A'
  require: 'ngModel'
  scope:
    dcMatch: "="
  link: (scope, elem, attrs, model) ->
    if (!attrs.dcMatch)
      console 'dcMatch expects a model as an argument!'
    model.$validators.dcMatch = (modelValue) ->
      modelValue == scope.dcMatch
    scope.$watch "dcMatch", () ->
      model.$validate()
