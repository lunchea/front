'use strict'

angular
.module 'lunchboxApp.common', [
  'lunchboxApp.auth'
  'lunchboxApp.settings'
  'lunchboxApp.services'
]

.directive "pagination", ->
  restrict: "EA"
  scope:
    totalPages: "="
    page: "=ngModel"
    ngChange: "&"
  require: "ngModel"
  templateUrl: "/components/common/pagination.html"
  link: (scope, element, attrs, ngModel) ->
    scope.noNext = () ->
      scope.page >= scope.totalPages

    scope.noPrevious = () ->
      scope.page <= 1

    makePage = (number, text, isActive) ->
      number: number
      text: text
      active: isActive

    getPages = (currentPage, totalPages) ->
      pages = []
      startPage = 1
      if !currentPage
        pages.push makePage(startPage, startPage, true)
      else
        pages.push makePage(currentPage, currentPage, true)
      for i in [1..2]
        if currentPage - i > 0
          pages.splice 0, 0, makePage(currentPage - i, currentPage - i, false)
        if currentPage + i <= totalPages
          pages.push makePage(currentPage + i, currentPage + i, false)

      pages

    scope.selectPage = (number) ->
      if number > 0 and number <= scope.totalPages
        ngModel.$setViewValue number
        scope.pages = getPages number, scope.totalPages
        scope.ngChange()

    scope.$watch 'page', (newValue, oldValue) ->
      scope.pages = getPages scope.page, scope.totalPages

.directive "payment", ->
  restrict: "EA"
  scope:
    order: "="
  templateUrl: "/components/common/payment.html"
  link: (scope, element, attrs) ->
    scope.purchase = () ->
      scope.order.$send
        method: 'POST'
        url: scope.order.$url() + "/purchase"
        data: scope.card
      , (response) ->
        element.html response.data
        console.log response
      , (error) ->
        element.html error.data
        console.log error
