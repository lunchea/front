'use strict'

###*
 # @ngdoc function
 # @name lunchboxApp.settings
 # @description
 # settings of the lunchboxApp
###
angular
.module 'lunchboxApp.settings', [
]

.constant 'settings',
  apiUrl: 'https://api.lunchea.com/api/v1'
  # apiUrl: 'http://localhost:3000/api/v1'
