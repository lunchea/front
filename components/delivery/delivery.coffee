'use strict'

###*
 # @ngdoc directive
 # @name lunchboxApp.directive: All Delivery-related directives
 # @description
 # #  All Delivery-related directives
###


angular
.module 'lunchboxApp.deliveries', [
  'lunchboxApp.common'
]

.directive "deliveryForm", [
  'Delivery', 'Stores', '$http', 'settings'
  (Delivery, Stores, $http, settings) ->
    restrict: "E"
    scope:
      store: "=ngModel"
    templateUrl: "/components/delivery/form.html"
    link: (scope) ->
      scope.getDeliveries = () ->
        url = settings.apiUrl + "/deliveries.json?store_id=" + scope.store.id
        $http(
          method: "GET"
          url: url
        ).success (data, status, headers, config) ->
          scope.deliveries = data
      scope.getDeliveries()
]

