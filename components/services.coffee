'use strict'

###*
 # @ngdoc function
 # @name lunchboxApp.services
 # @description
 # services of the lunchboxApp
###
angular
.module 'lunchboxApp.services', [
  'lunchboxApp.settings'
  'satellizer'
  'ngStorage'
  'restmod'
]
.config [
  '$authProvider', 'settings'
  ($authProvider, settings) ->
    $authProvider.loginOnSignup = true
    $authProvider.loginUrl = settings.apiUrl + '/auth/login'
    $authProvider.signupUrl = settings.apiUrl + '/auth/signup'
    $authProvider.authHeader = 'x-jwt'
    $authProvider.tokenPrefix = 'satellizer'
]

.config [
  'restmodProvider'
  'settings'
  (restmodProvider, settings) ->
    restmodProvider.rebase
      $config:
        style: "Scixiv"
        primaryKey: "id"
        urlPrefix: settings.apiUrl
      $hooks:
        "after-save-error": (res) ->
          console.log res
        "after-destroy-error": (res) ->
          console.log res

]

.filter "lunchOrDinner", ()->
  return (date_string)->
    date = new Date(date_string)    # date is automatically in EST
    hour = date.getHours()
    if hour < 13
      return "Lunch"
    else
      return "Dinner"

.factory 'User', [
  "restmod", "StoreOwned"
  (restmod, StoreOwned) ->
    restmod.model('/users').mix
      stores:
        init: StoreOwned.$collection
          store_admin_id: @$pk
]

.factory 'currentUser',[
  "$localStorage", "User"
  ($localStorage, User) ->
    initialize = (user) ->
      $localStorage.currentUser = user if user
      console.log 'user', user
      currentUser = User.$buildRaw user || $localStorage.currentUser
      currentUser.$on "after-save", () ->
        $localStorage.currentUser = @
      currentUser

    currentUser = initialize()

    return {
      init: (user) ->
        initialize user
      get: () ->
        currentUser
    }
]

.factory 'MoneyParseFilter', () ->
  (_value) ->
    if _value
      _value.fractional / 100.0
    else
      0

.factory 'PreferencesParseFilter', [
  'BoxPreference'
  (BoxPreference) ->
    (_collection) ->
      BoxPreference.$collection().$decode _collection
]

.factory 'DeliveriesParseFilter', [
  'Delivery'
  (Delivery) ->
    (_collection) ->
      Delivery.$collection().$decode _collection
]

.factory 'OrderItemsParseFilter', [
  'OrderItem'
  (OrderItem) ->
    (_collection) ->
      OrderItem.$collection().$decode _collection
]

.factory 'Box', [ 'restmod', 'BoxPreference', (restmod, BoxPreference) ->
  restmod.model('/boxes').mix  'TotalCountModel', 'PagedModel',
    category:
      init: 'general'
    price:
      decode: 'MoneyParse'
    preferences:
      init: BoxPreference.$collection()
      decode: 'PreferencesParse'
    $hooks:
      'before-save': (_req) ->
        _req.data['preferences_attributes'] = _req.data['preferences']
    $extend:
      Record:
        newPreference: () ->
          @preferences.$build().$reveal true
        expired: () ->
          # console.log '@expired_at ', @expired_at
          # @expired_at
          true
        destroy: () ->
          @_destroy = true
]

.factory 'BoxPreference', [ 'restmod', (restmod) ->
  restmod.model('/box_preferences').mix
    $extend:
      Record:
        destroy: () ->
          @_destroy = true
]

.factory "ItemPreference", [ 'restmod', (restmod) ->
  restmod.model('/item_preferences').mix
    preference:
      init: () ->
        []
    max_num_of_pref:
      init: 2
    $extend:
      Record:
        # Use to configure this preference instance.
        config: (restrictions)->
          # Do something with restrictions
          return

        addPref: (option)->
          if @preference.length < @max_num_of_pref
            option.box_preference_id = option.id
            @preference.push(option)

        removePref: (option)->
          if @preference.length > 0
            @preference.splice @preference.indexOf(option), 1
 ]

.factory 'OrderItem', [ 'restmod', 'Box', (restmod, Box) ->
  restmod.model('/order_items').mix
    quantity:
      init: 1
    preferences:
      init: () ->
        []
    max_preferences_number:
      init: 0
    $extend:
      Record:
        getBox: (box) ->
          @box = Box.$buildRaw box
          @box_id = box.id
          @max_preferences_number = box.max_preferences_number

        price: ->
          if @box
            Number(@box.price) * Number(@quantity)
          else
            0
        plusOne: ->
          @quantity += 1

        minusOne: ->
          if @quantity > 0
            @quantity -= 1
          else
            @quantity = 0

        prefCount: (preference)->
          prefs = _.filter @preferences, {box_preference_id: preference.id}
          prefs.length

        addPref: (preference)->
          console.log 'sfhi.', @max_preferences_number
          if @preferences.length < @max_preferences_number
            @preferences.push
              box_preference_id: preference.id
              value: preference.name
          else
            @preferences = _.reject @preferences,
              box_preference_id: preference.id

        removePref: (option)->
          index = _.findIndex @preferences,
            box_preference_id: option.id

          @preferences.splice index, 1

        hasSelected: (preference)->
          pref = _.find @preferences,
            box_preference_id: preference.id

        pref_count: (option)->
          tmp = _.filter @preferences,
            box_preference_id: option.id
          tmp.length
]

.factory 'Delivery', [
  'restmod', "$q", 'OrderItem', 'Box', "Store"
  "$http", "settings", "ItemPreference", "$rootScope"
  ( restmod, $q, OrderItem, Box, Store, $http, settings,
    ItemPreference, $rootScope ) ->
    # decorate the $delegate
    restmod.model('/deliveries').mix
      items:
        init: () ->
          OrderItem.$collection()
        decode: "OrderItemsParse"
      items_meta:
        init: () ->
          {}      # Record all selected boxes id and quantity
      delivered_at:
        init: ''
      boxes:
        init: undefined
      categories:
        init: () ->
          []
      confirmed_at:
        init: ''
      store_id:
        init: undefined
      $extend:
        Record:
          hasPreferences: ()->
            for item in @items
              if item.max_preferences_number > 0
                return true
            false

          hasUnsaturatedPreferences: ()->
            for item in @items
              if item.box and
              item.preferences.length < item.max_preferences_number
                return true
            return false

          addEmptyItem: () ->
            @items.$build()

          addItem: (box) ->
            if (item = _.find @items, {box_id: box.id}) and
              (item.max_preferences_number < 1)
                item.quantity += 1
                console.log "@items", @items
                console.log "@totalCost()", @totalCost()
            else
              item = @addEmptyItem()
              item.getBox(box)
              @items_meta[box.id] = item
            item

          # Only when the corresponding box has no preferences
          # can the selected entries can be removed
          # otherwise, should use the remove button of each preference
          removeItem: (box) ->
            if item = _.find @items, {box_id: box.id}
              if item.quantity > 1
                item.quantity -= 1
              else if item.quantity == 1
                @items.splice @items.indexOf(item), 1
                delete @items_meta[box.id]

          totalCost: ()->
            totalCost = 0
            if @items?
              for item in @items
                totalCost += item.price()
              return totalCost

          getMenu: (successHandler)->
            delivery = @
            @items.$clear()
            @categories = []
            # =========================================================
            # Mock.js test
            # response_mock = MockTest.boxes_mock
            # console.log '--> Downloaded menu', response_mock
            # delivery.boxes = (box for box in response_mock when box.store_id
            # is delivery.store_id)
            # if delivery.boxes and delivery.boxes.length > 0
            #   console.log "successfully obtained menu"
            #   for box in delivery.boxes
            #     if box.category not in delivery.categories
            #       delivery.categories.push(box.category)
            #   if successHandler?
            #     successHandler()
            # ==========================================================
            # for getting boxes
            # Get the boxes
            @boxes = @store.boxes
            @sortedBoxes = _.groupBy @boxes, 'category'
            if delivery.boxes and delivery.boxes.length > 0
              console.log "successfully obtained menu"
              # Build the <delivery.categories, array>
              # from the returned data
              for box in delivery.boxes
                if box.category == ""
                  box.category = "Other"
                if box.category not in delivery.categories
                  delivery.categories.push(box.category)
              if successHandler?
                successHandler()
            # =========================================================
            # ========= Mock.js test =============
            # response_mock = MockTest.boxes_mock
            # console.log '--> Downloaded menu', response_mock
            # delivery.boxes = (box for box in response_mock when
            # box.store_id is delivery.store_id)
            # if delivery.boxes and delivery.boxes.length > 0
            #   console.log "successfully obtained menu"
            #   for box in delivery.boxes
            #     if box.category not in delivery.categories
            #       delivery.categories.push(box.category)
            #   if successHandler?
            #     successHandler()
            # ==========================================================


          pickStore: (store) ->
            @delivered_at = undefined
            # only when selecting a new store
            # will this function move on.
            if store.id isnt @store_id
              today = new Date()
              todayDate = today.getDate()
              holidays = [
                '11/27/14','11/28/14','12/24/14'
                '12/25/14','12/31/14',  '1/1/15'
                 '1/26/15', '1/27/15', '1/28/15', '2/9/15'
              ]
              store_email_time = new Date(store.email_at)
              i = 0
              tmp = (store_email_time.getHours()+1)*60+store_email_time.getMinutes() #+1 hour due to EDT
              if (today.getHours()*60+today.getMinutes() >= tmp)
                i = 1
              dates = []
              j = 0
              is_diff = !store.dates
              while j < 6
                date = new Date()
                date.setDate(todayDate + i)
                dateString = (date.getMonth() + 1) + '/' + (date.getDate()) +
                            '/' + (date.getFullYear() % 100)
                # Exclude Sundays and Saturdays and public holidays
                if date.getDay() % 6 != 0 and holidays.indexOf(dateString) == -1
                  dates.push(date)
                  if !is_diff
                    if date.getDay() != store.dates[j].getDay()
                      is_diff = true
                  j++
                i++
              if is_diff
                store.dates = dates
              @store = store
              @store_id = store.id


          ## When clicking a date, set $scope.delivered_at to specified date
          pickDate: (date, pickDateSuccessHandler) ->
            if @delivered_at != date

              ## Renew the Delivery object every time, to kick out
              # existing .boxes

              store_delivery_time = new Date(@store.delivered_at)

              # extract the time-of-@ from server data
              # use to display in the INFO section
              @_hour = store_delivery_time.getHours()
              @_min = store_delivery_time.getMinutes()
              new_date = new Date(date.toDateString())
              # to prevent circular date structure
              new_date.setHours(@_hour)
              new_date.setMinutes(@_min)
              @delivered_at = new_date
              ## remove previous selection data when switching data
              @items_meta = {}
              @getMenu()  ## Get the menu
              # If getMenu does not succeed in 5 second,
              # error message will show
              @store.getSaleOf(@delivered_at) if @store.min_delivery_sale > 0
              @$reveal true

          isSelected: (box) ->
            _.find @items, {box_id: box.id}

          ready: ->
            return @boxes and @boxes.length > 0 and @delivered_at and
            @store_id == @boxes[0].store_id

]

.factory 'Order', [
  'restmod',
  '$timeout',
  'OrderItem',
  'Delivery',
  'Box',
  (restmod, $timeout, OrderItem, Delivery, Box) ->
    restmod.model('/orders', 'PagedModel', 'DebouncedModel').mix
      deliveries:
        init: () ->
          Delivery.$collection()
        decode: 'DeliveriesParse'
      email:
        init: ''
      phone:
        init: ''
      threshhold:
        init: 1000
      threshhold_pattern:
        init: () ->
          [/@stonybrook.edu$/]
      source:
        init: "lunchea"
      # Three properties to indicate submission status
      in_submission:
        init: false  # true after submit()
      submitted:
        init: false      # true after successful $save()
      success:
        init: false        # true after submitSuccessHandler()
      $hooks:
        'before-save': (_req) ->
          @in_submission = true
          _req.data['deliveries_attributes'] = _req.data['deliveries']
          for delivery in _req.data['deliveries_attributes']
            delivery.items_attributes = delivery.items
            for item in delivery.items_attributes
              item.preferences_attributes = item.preferences
          console.log "order", _req.data
        'after-save': (response) ->
          console.log "$save success response", response
          @submitted = true
          @success = true
        'after-save-error': (response) ->
          @in_submission = false
          console.log response

      $extend:
        Record:
          addDelivery: () ->
            deli = @deliveries.$build()
            deli

          removeDelivery: (index, switchToDelivery) ->
            if @deliveries.length > 1
              @deliveries.splice index, 1
            if index > 0
              switchToDelivery(index - 1)

          totalCost: () ->
            if @submitted
              return
            total_cost = 0
            if @deliveries?
              for delivery in @deliveries
                total_cost += delivery.totalCost()
            total_cost

          hasEmptyOrder: () ->
            for delivery in @deliveries
              if delivery.totalCost() == 0
                return true
              if delivery.hasUnsaturatedPreferences()
                return true
            return false

          hasLateDelivery: () ->
            today = new Date()
            for delivery in @deliveries
              delivery_date = new Date(delivery.delivered_at)
              email_time = new Date(delivery.store.email_at)
              email_time.setFullYear(delivery_date.getFullYear())
              email_time.setMonth(delivery_date.getMonth())
              email_time.setDate(delivery_date.getDate())
              email_time.setHours(email_time.getHours()+1)
              if today > email_time
                return true
            return false

          checks: (errorMsgHandler) ->
            ###
              Perform sanity checks of the order:
            ###
            console.log "checks"
            if @in_submission
              console.log "already in submission"
              return false
            if @hasLateDelivery()
              console.log "late_order"
              errorMsgHandler(true, 'late_order')
              return false
            if @hasEmptyOrder()
              console.log "empty_order"
              errorMsgHandler(true, 'empty_order')
              return false
            if @totalCost() > @threshhold
              for pattern in @threshhold_pattern
                if pattern.test(@email)
                  return true
              errorMsgHandler(true, 'exceeded')
              return false
            return true

          removeEmpty: () ->
            temp = []
            for delivery in @deliveries
              if delivery.totalCost() > 0
                temp.push delivery
            @deliveries = temp

          confirm: () ->
            @$send
              method: 'PUT'
              url: @$url() + "/confirm.json"
              data:
                access_token: @access_token
            , (response) ->
              @confirmed_at = response.data.confirmed_at



          cancel: () ->
            @$send
              method: "DELETE"
              url: @$url() + "/cancel"
              data:
                access_token: @access_token
            , (response) ->
              @confirmed_at = response.data.confirmed_at


]
.factory 'OrdersManager', ["$http", "settings", ($http, settings) ->
  confirm: (order) ->
    $http(
      method: 'PUT'
      url: settings.apiUrl + "/orders/" + order.id + "/confirm.json"
      data:
        access_token: order.access_token
    )

  cancel: (order) ->
    $http(
      method: "DELETE"
      url: settings.apiUrl + "/orders/" + order.id + "/cancel"
      data:
        access_token: order.access_token
    )
]

.factory 'Store', [
  "restmod", 'MoneyParseFilter'
  (restmod, MoneyParseFilter) ->
    restmod.model('/stores').mix
      "min_delivery_sale":
        decode: 'MoneyParse'
      $extend:
        Record:
          confirm: () ->
            @$send
              method: 'PUT'
              url: @$url() + "/confirm"
            , (response) ->
              console.log 'response', response
              @confirmed_at = response.data.confirmed_at
            , (err) ->
              console.log 'err', err

          cancel: () ->
            @$send
              method: "DELETE"
              url: @$url() + "/cancel"
            , (response) ->
              console.log 'response', response
            , (err) ->
              console.log 'err', err
          getSaleOf: (date) ->
            @$send
              method: "GET"
              url: @$url() + "/sale?date=" + date
              data:
                date: date
            , (response) ->
              console.log 'response', response
              @sale = MoneyParseFilter response.data
            , (err) ->
              console.log 'err', err
]

.factory 'StoreOwned', [
  "restmod", 'Store'
  (restmod, Store) ->
    restmod.model('/stores/owned').mix 'Store'
]

.factory 'Stores', [
  "Store"
  (Store) ->
    collection = Store.$search()
    {
      get: collection
    }

]


angular.module('restmod').factory 'TotalCountModel', [
  'restmod'
  (restmod) ->
    restmod.mixin ()->
      @setProperty('total', 'X-Total')
      @setProperty('perPage', 'X-Per-Page')
      .on 'after-fetch-many', (_response) ->
        total = _response.headers(@$type.getProperty('total'))
        perPage = _response.headers(@$type.getProperty('perPage'))
        @$total = if total? then parseInt(total, 10) else 0
        @$perPage = if perPage? then parseInt(perPage, 10) else 0
]
