'use strict'

###*
 # @ngdoc directive
 # @name lunchboxApp.directive: All Box-related directives
 # @description
 # #  All Box-related directives
###


angular
.module 'lunchboxApp.boxes', [
  'lunchboxApp.common'
]

.directive "boxForm", [
  'Stores'
  (Stores) ->
    restrict: "E"
    scope:
      box: "=ngModel"
    templateUrl: "/components/boxes/form.html"
    link: (scope) ->
      scope.stores = Stores.get
]

.directive "boxFormUser", [
  'Box', '$stateParams', '$location'
  (Box, $stateParams, $location) ->
    restrict: "E"
    scope:
      store: "=ngModel"
    templateUrl: "/components/boxes/formpages.html"
    link: (scope) ->
      scope.page = ()->
        $location.search
          page: scope.boxes.$page
        $scope.boxes.$refresh
          page: scope.boxes.$page

      scope.boxes = scope.store.boxes
      # console.log("hello1")
      # console.log(box)
      # scope.boxes.$on 'after-fetch-many', (res) ->
      #   scope.sortedBoxes = _.groupBy this, 'category'
      # scope.boxes.$on 'after-save', () ->
      #   scope.sortedBoxes = _.groupBy $scope.boxes, 'category'
      # scope.boxes.$fetch()
      scope.sortedBoxes = _.groupBy scope.boxes, 'category'
      scope.newBox = () ->
        if scope._box
          delete  scope._box
        else
          scope._box = scope.boxes.$new()
          scope._box.$on 'after-save', () ->
            delete  scope._box
    
]

.directive "boxFormCard", [
  () ->
    restrict: "E"
    scope:
      box: "=ngModel"
    templateUrl: "/components/boxes/formuser.html"
]