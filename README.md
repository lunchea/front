lunchbox
========

# Views

# Controllers

  - ## `<LunchboxCtrl, controller>`: order page controller
    - Location: scripts/controllers/order.coffee
    - Purpose: control the user interface

# Services

  - ## `<OrdersManager, factory>`
    - Location: 
    - Purpose: 

# Decorators

  scripts/decorators/

# Directives
  
  - ## Orders-related
    - Location: components/orders/
  - ## Authentication-related
    - Location: components/auth/
