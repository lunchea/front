'use strict'

###*
 # @ngdoc service
 # @name scixivApp.auth
 # @description
 # # auth
 # Factory in the scixivApp.
###

angular.module('lunchboxApp')
  .factory 'Menu', () ->
    categories =  [
      {
        "name": "Special",
        # "description": "Comes with a chicken soup.",
        "keywords": "special",
      },
      {
        "name": "Chicken",
        # "description": "Comes with a chicken soup.",
        "keywords": "chicken, soup, spicy",
      },
      {
        "name": "Beef",
        # "description": "Comes with a beef and tofu soup.",
        "keywords": "beef, tofu, soup, spicy",
      },
      {
        "name": "Pork",
        # "description": "Comes with a tofu soup.",
        "keywords": "Pork, soup",
      },
      {
        "name": "Noodle",
        # "description": "Comes with a tofu soup.",
        "keywords": "noodle",
      }
    ]

    options = [
      {
          "id": 0,
          "name": "Daily Sepcial",
          "chinese_name": "当日特价",
          "price":{
              "fractional": 600,
          },
          "preferences": [],
      },
      {
          "id": 1,
          "name": "Pork Ribs",
          "chinese_name": "排骨",
          "price":{
              "fractional": 600,
          },
          "preferences": [],
      },
      {
          "id": 2,
          "name": "Boneless Ribs",
          "chinese_name": "无骨排",
          "price":{
              "fractional": 600,
          },
          "preferences": [],
      },
      {
          "id": 3,
          "name": "Khong Pork",
          "chinese_name": "控肉/扣肉",
          "price":{
              "fractional": 600,
          },
          "preferences": [],
      },
      {
          "id": 4,
          "name": "Double Sauteed Pork",
          "chinese_name": "回锅肉",
          "price":{
              "fractional": 600,
          },
          "preferences": [],
      },
      {
          "id": 5,
          "name": "Cumin Sliced Pork",
          "chinese_name": "孜然肉",
          "price":{
              "fractional": 600,
          },
          "preferences": [],
      },
      {
          "id": 6,
          "name": "Shredded Pork in Garlic Sauce",
          "chinese_name": "鱼香肉丝",
          "price":{
              "fractional": 600,
          },
          "preferences": [],
      },
      {
          "id": 7,
          "name": "Beef Stew",
          "chinese_name": "牛腩",
          "price":{
              "fractional": 600,
          },
          "preferences": [],
      },
      {
          "id": 8,
          "name": "Curry Beef",
          "chinese_name": "咖喱牛",
          "price":{
              "fractional": 600,
          },
          "preferences": [],
      },
      {
          "id": 9,
          "name": "Beef Broccoli",
          "chinese_name": "芥兰牛",
          "price":{
              "fractional": 600,
          },
          "preferences": [],
      },
      {
        "id": 10,
        "name": "Curry Chicken",
        "chinese_name": "咖喱鸡",
        "price":{
            "fractional": 600,
        },
        "preferences": [],
      },
      {
        "id": 11,
        "name": "Chicken Broccoli",
        "chinese_name": "芥兰鸡",
        "price":{
            "fractional": 600,
        },
        "preferences": [],
      },
      {
        "id": 12,
        "name": "General Tso’s Chicken",
        "chinese_name": "左宗鸡",
        "price":{
            "fractional": 600,
        },
        "preferences": [],
      },
      {
        "id": 13,
        "name": "Spicy Crispy Chicken",
        "chinese_name": "辣子鸡",
        "price":{
            "fractional": 600,
        },
        "preferences": [],
      },
      {
        "id": 14,
        "name": "Mixed Vegetables",
        "chinese_name": "素什锦",
        "price":{
            "fractional": 600,
        },
        "preferences": [],
      },
      {
        "id": 15,
        "name": "Mapo Tofu",
        "chinese_name": "麻婆豆腐",
        "price":{
            "fractional": 600,
        },
        "preferences": [],
      },
      {
        "id": 16,
        "name": "Black Bean Tofu",
        "chinese_name": "豆豉豆腐",
        "price":{
            "fractional": 600,
        },
        "preferences": [],
      },
      {
        "id": 17,
        "name": "Sesame Tofu",
        "chinese_name": "芝麻豆腐",
        "price":{
            "fractional": 600,
        },
        "preferences": [],
      },
      {
        "id": 18,
        "name": "Shredded Pork With Pickled Cabbage",
        "chinese_name": "榨菜肉丝",
        "price":{
            "fractional": 600,
        },
        "preferences": [],
      },
      {
        "id": 19,
        "name": "Shredded Pork With Dry Bean Curd",
        "chinese_name": "香干肉丝",
        "price":{
            "fractional": 600,
        },
        "preferences": [],
      },
      {
        "id": 20,
        "name": "Shredded Beef With Pepper",
        "chinese_name": "小辣椒牛肉丝",
        "price":{
            "fractional": 600,
        },
        "preferences": [],
      },
      {
        "id": 21,
        "name": "Beef Noodle Soup",
        "chinese_name": "牛肉汤面",
        "price":{
            "fractional": 600,
        },
        "preferences": [],
      },
      {
        "id": 22,
        "name": "Winter Cabbage Noodle Soup",
        "chinese_name": "雪菜肉丝汤面",
        "price":{
            "fractional": 600,
        },
        "preferences": [],
      },
      {
        "id": 23,
        "name": "Pickled Cabbage Noodle Soup",
        "chinese_name": "榨菜肉丝汤面",
        "price":{
            "fractional": 600,
        },
        "preferences": [],
      },
      {
        "id": 24,
        "name": "Taiwanese Fried Noodle",
        "chinese_name": "台式炒面",
        "price":{
            "fractional": 800,
        },
        "preferences": [],
      },
      {
        "id": 25,
        "name": "Taiwanese Fried Rice Noodle",
        "chinese_name": "台式炒米粉",
        "price":{
            "fractional": 800,
        },
        "preferences": [],
      },
      {
        "id": 26,
        "name": "Taiwanese Chicken",
        "chinese_name": "台式鸡",
        "price":{
            "fractional": 800,
        },
        "preferences": [],
      },
      {
        "id": 27,
        "name": "Taiwanese Pork Ribs",
        "chinese_name": "台式排骨",
        "price":{
            "fractional": 800,
        },
        "preferences": [],
      },
      {
        "id": 28,
        "name": "Taiwanese Khong Pork",
        "chinese_name": "台式扣肉",
        "price":{
            "fractional": 800,
        },
        "preferences": [],
      },
      {
          "id": 29,
          "name": "Taiwanese Fish",
          "chinese_name": "台式鱼",
          "price":{
              "fractional": 800,
          },
      },
      {
        "id": 30,
        "name": "Beef Stew Over Rice",
        "chinese_name": "牛腩盖饭",
        "price":{
            "fractional": 800,
        },
        "preferences": [],
      },
      {
        "id": 31,
        "name": "5 Spiced Roast Beef In Brown Sauce ",
        "chinese_name": "五香牛",
        "price":{
            "fractional": 800,
        },
        "preferences": [],
      },
    ]
    menu = {
      "options": options,
      "categories": categories,
    }





    return menu
