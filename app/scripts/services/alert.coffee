'use strict'

###*
 # @ngdoc service
 # @name scixivApp.auth
 # @description
 # # auth
 # Factory in the scixivApp.
###
console.log "alert.coffee"
angular.module('lunchboxApp')
  .factory 'Alert', ["$rootScope", "$timeout", ($rootScope, $timeout) ->
    clear = ()->
      console.log "clearing"
      $rootScope.alert.show = false
      $timeout ()->
        $rootScope.alert.msg = undefined
        $rootScope.alert.error = undefined
      , 500
    showAlert = (delay) ->
      $rootScope.alert.show = true
      $timeout clear, delay

    return {
      showMsg: (content, delay) ->
        delay = if delay? then delay else 2000
        $rootScope.alert.msg = content
        showAlert(delay)

      showError: (content, delay) ->
        delay = if delay? then delay else 10000
        $rootScope.alert.error = content
        showAlert(delay)

      closeAlert: clear
    }
  ]
  .directive "alert", ["$rootScope", "Alert", ($rootScope, Alert)->
    restrict: "E"
    templateUrl: "/components/alert.html"
    link: (scope, element, attrs) ->
      $rootScope.alert = {
        show: false
        msg: undefined
        error: undefined
      }
      scope.closeAlert = Alert.closeAlert
  ]