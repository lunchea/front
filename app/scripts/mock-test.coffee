'use strict'
###*
 # @ngdoc overview
 # @name lunchboxApp
 # @description
 # # lunchboxApp
 #
 # Main module of the application.
###
angular
  .module 'lunchboxApp'
  .factory "MockTest", () ->

    # Simulating box.where()
    box_template = {
      "name|1-20":"shit "
      "chinese_name|1-20": "屎"
      "id|1024-4096": 100
      "price": {
        "fractional|100-10000": 100
      }
      "store_id|1-3": 100
    }

    boxes_template = {
      "boxes|20": [box_template]
    }

    store_template = {
      "name|1-4":"store"
      "id|+1": 1
    }

    stores_template = {
      "stores|3": [store_template]
    }

    stores_mock = Mock.mock(stores_template).stores
    boxes_mock = Mock.mock(boxes_template).boxes

    return {
      stores_mock: stores_mock
      boxes_mock: boxes_mock
    }
