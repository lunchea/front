# jshint devel:true
'use strict'

###*
 # @ngdoc overview
 # @name lunchboxApp
 # @description
 # # lunchboxApp
 #
 # Main module of the application.
###
angular
.module 'lunchboxApp', [
  'ngAnimate'
  'ngSanitize'
  'ngTouch'
  'ui.router'
  'ui.gravatar'
  'angular-loading-bar'
  'ngMaterial'
  'ngMessages'
  'restmod'
  'ngStorage'
  'lunchboxApp.services'
  'lunchboxApp.auth'
  'lunchboxApp.settings'
  'lunchboxApp.common'
  'lunchboxApp.orders'
  'lunchboxApp.boxes'
  'lunchboxApp.stores'
  'lunchboxApp.deliveries'
]

.config [
  'gravatarServiceProvider'
  (gravatarServiceProvider) ->
    gravatarServiceProvider.defaults =
      size: 100
      default: 'mm'  # Mystery man as default for missing avatars

    gravatarServiceProvider.secure = true
]

.config [
  '$authProvider', 'settings'
  ($authProvider, settings) ->
    $authProvider.loginRoute = '/login'
    $authProvider.signupRoute = '/signup'
    $authProvider.logoutRedirect = '/logout'
]