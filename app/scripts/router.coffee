'use strict'

###*
 # @ngdoc overview
 # @name lunchboxApp
 # @description
 # # lunchboxApp
 #
 # Main module of the application.
###
angular
  .module 'lunchboxApp'
  .config [
    '$stateProvider'
    'cfpLoadingBarProvider'
    '$urlRouterProvider'
    ($stateProvider, cfpLoadingBarProvider, $urlRouterProvider) ->

      cfpLoadingBarProvider.latencyThreshold = 50
      $urlRouterProvider.otherwise('/')

      $stateProvider
        .state 'root',
          url: '/'
          templateUrl: '/views/index.html'
        .state 'feedback',
          url: '/feedback'
          templateUrl: '/views/feedback.html'
        .state 'faq',
          url: '/faq'
          templateUrl: '/views/faq.html'
        .state 'rpartner',
          url: '/rpartner'
          templateUrl: '/views/rpartner.html'
        .state 'pricerefundpolicy',
          url: '/pricerefundpolicy'
          templateUrl: '/views/pricerefundpolicy.html'
        .state 'privacypolicy',
          url: '/privacypolicy'
          templateUrl: '/views/privacypolicy.html'
        .state 'joinus',
          url: '/joinus'
          templateUrl: '/views/joinus.html'
        .state 'about',
          url: '/about?page'
          templateUrl: '/views/about.html'
        .state 'orders',
          url: '/orders'
          template: '<orders ui-view=""></orders>'
          reloadOnSearch: false
        .state 'orders.new',
          url: '/new/:external'
          templateUrl: '/views/orders/new.html'
          controller: 'LunchboxCtrl'
          reloadOnSearch: false
        .state 'orders.index',
          url: '/display'
          templateUrl: '/views/orders/index.html'
          controller: 'displayOrderCtrl'
        .state 'order',
          abstract: true,
          url: '/orders/:id'
          template: '<div ui-view=""></div>'
        .state 'order.show',
          url: ''
          templateUrl: '/views/orders/success.html'
          controller: 'OrderConfirmationCtrl'
        .state 'order.confirm',
          url: '/confirm?access_token'
          templateUrl: '/views/orders/show.html'
          controller: 'OrderConfirmationCtrl'

        # user dashboard

        .state 'dashboard',
          url: '/dashboard'
          templateUrl: '/views/users/show.html'
          controller: "UserCtrl"

        .state 'dashboard.orders',
          url: '/orders'
          templateUrl: '/views/orders/userorders.html'
          controller: 'displayUserOrderCtrl'

        .state 'dashboard.deliveries',
          url: '/deliveries'
          templateUrl: '/views/orders/storedeliveries.html'
          controller: 'displayStoreDeliveryCtrl'


        # Site Admin
        .state 'dashboard.stores',
          url: '/stores'
          abstract: true
          template: '<div ui-view></div>'
          controller: 'UserStoresCtrl'

        .state 'dashboard.stores.list',
          url: '/storeslist'
          templateUrl: '/views/users/stores.html'
          controller: 'UserStoresCtrl'

        .state 'dashboard.stores.new',
          url: '/stores'
          template: '''
            <store-form ng-model="store"></store-form>
            '''
          controller: 'UserNewStoreCtrl'

        .state 'admin',
          url: "/admin"
          templateUrl: "/views/admin/main.html"

        .state 'admin.boxes',
          url: "/boxes?page"
          reloadOnSearch: false
          templateUrl: "/views/admin/boxes.html"
          controller: [
            '$scope', 'Box', '$stateParams', '$location'
            ($scope, Box, $stateParams, $location) ->
              $scope.page = ()->
                $location.search
                  page: $scope.boxes.$page
                $scope.boxes.$refresh
                  page: $scope.boxes.$page

              $scope.boxes = Box.$collection()
              $scope.boxes.$on 'after-fetch-many', (res) ->
                $scope.sortedBoxes = _.groupBy this, 'category'
              $scope.boxes.$on 'after-save', () ->
                $scope.sortedBoxes = _.groupBy $scope.boxes, 'category'
              $scope.boxes.$fetch()
              $scope.newBox = () ->
                if $scope._box
                  delete  $scope._box
                else
                  $scope._box = $scope.boxes.$new()
                  $scope._box.$on 'after-save', () ->
                    delete  $scope._box

          ]

        .state 'admin.stores',
          url: "/stores?page"
          reloadOnSearch: false
          templateUrl: "/views/admin/stores.html"
          controller: [
            '$scope', 'Stores', '$stateParams', '$location', 'currentUser'
            ($scope, Stores, $stateParams, $location, currentUser) ->
              $scope.page = ()->
                $location.search
                  page: $scope.stores.$page
                $scope.stores.$refresh
                  page: $scope.stores.$page

              $scope.stores = $scope.currentUser.get().stores
              $scope.stores.$resolve()
              $scope.newStore = () ->
                if $scope._store
                  delete  $scope._store
                else
                  $scope._store = $scope.stores.$new()
                  $scope._store.$on 'after-save', () ->
                    delete  $scope._store

          ]

        .state 'admin.orders',
          url: "/orders?page"
          reloadOnSearch: false
          templateUrl: "/views/admin/orders.html"
          controller: [
            '$scope', 'Order', 'Store', '$stateParams', '$location'
            ($scope, Order, Store, $stateParams, $location) ->
              $scope.page = ()->
                $location.search
                  page: $scope.orders.$page
                $scope.orders.$refresh
                  page: $scope.orders.$page

              $scope.orders = Order.$search()
              $scope.stores = Store.$search().$then ()->
                $scope.stores_info = {}
                for store in $scope.stores
                  $scope.stores_info[store.id] = store.name
          ]

        .state 'admin.users',
          url: "/users?page"
          reloadOnSearch: false
          templateUrl: "/views/admin/users.html"
          controller: [
            '$scope', 'User', '$stateParams', '$location'
            ($scope, User, $stateParams, $location) ->
              $scope.page = ()->
                $location.search
                  page: $scope.users.$page
                $scope.users.$refresh
                  page: $scope.users.$page

              $scope.users = User.$collection()
              $scope.users.$fetch()
              $scope.newUser = () ->
                if $scope._user
                  delete  $scope._user
                else
                  $scope._user = $scope.users.$new()
                  $scope._user.$on 'after-save', () ->
                    delete  $scope._user

          ]
        # authentication

        .state 'login',
          url: '/login'
          onEnter: ['$auth', '$window', ($auth, $window) ->
            $window.history.back() if $auth.isAuthenticated()
          ]
          template: '''
            <login style="width:100%" t-margin-40></login>
            <div layout="row" layout-align="center start" flex layout-fill>
            <span><a ui-sref="signup"><b>Sign Up</b></a>
            &nbsp&nbsp&nbsp
            <a ui-sref="resetRequest"><b> Forget your password?</b></a></span>
            </div>
          '''

        .state 'logout',
          url: '/logout'
          controller: [ "currentUser", "$state", (currentUser, $state) ->
            for member in Object.getOwnPropertyNames(currentUser)
              delete currentUser[member]
            $state.go 'root'
          ]

        .state 'signup',
          url: '/signup'
          onEnter: ['$auth', '$state', ($auth, $state) ->
            $state.go 'root' if $auth.isAuthenticated()
          ]
          template: '<sign-up style="width:100%" t-margin-40></sign-up>'

        .state 'resetRequest',
          url: '/reset_request'
          template: '''
            <div layout="row" layout-align="center start" flex layout-fill>
              <div layout="column" layout-align="start center">
                <div class="height-100"></div>
                <send-reset-email></send-reset-email>
              </div>
            </div>
            '''

        .state 'reset',
          url: '/reset?reset_password_token'
          template: '''
            <div layout="row" layout-align="center start" flex layout-fill>
              <div layout="column" layout-align="start center">
                <div class="height-100"></div>
                <reset-password reset-password-token="resetPasswordToken"
                after-save="back()">
                </reset-password>
              </div>
            </div>
            '''
          controller: [
            '$scope', '$stateParams', '$state'
            ($scope, $stateParams, $state) ->
              $scope.resetPasswordToken = $stateParams.reset_password_token
              $scope.back = () ->
                $state.go 'login'
          ]

        .state 'main',
          url: '/'
          templateUrl: '/views/index.html'
          controller: 'LunchboxCtrl'

        .state 'confirm_email',
          url: '/auth/confirm?confirmation_token'
          template: '<confirm-email confirm-token="token"></confirm-email>'
          controller: ["$scope", "$stateParams", ($scope, $stateParams) ->
            $scope.token = $stateParams.confirmation_token
          ]
  ]
