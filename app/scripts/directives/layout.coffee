'use strict'

###*
 # @ngdoc directive
 # @name lunchboxApp.directive:layout
 # @description
 # # layout
###
angular.module('lunchboxApp')
.directive 'navbar', [
  '$auth', '$state', 'currentUser', '$stateParams', '$rootScope'
  ($auth, $state, currentUser, $stateParams, $rootScope) ->
    templateUrl: '/views/directives/navbar.html',
    restrict: 'E',
    replace: true,
    transclude: true,
    link: (scope, element) ->
      scope.auth = $auth
      scope.from = ""
      $rootScope.$on "fromOutside", (event, data) ->
        scope.from = data.from
      scope.route = () ->
        if $auth.isAuthenticated()
          $state.go 'dashboard'
        else
          $state.go 'signup'

      scope.currentUser = currentUser
  ]

# Detect url parameter [:from], if there is no <from parameter>, display
# navbar
.directive "conditionalShow", ["$timeout", ($timeout)->
  restrict: "A"
  link: (scope, element)->
    console.log "from", scope.from
    # Show the navbar only when there is empty [:from] para
    if scope.from == ""
      element.css({"display": ""})

    # In rare cases, AngularJS is unable to grap the parameter soon
    # enough, so we have to set up a $watch
    unwatch = scope.$watch "from", ()->
      if scope.from != ""
        element.css({"display": "none"})
        unwatch()

    # If the page is loaded for a long time (10s), it is not necessary
    # to continue watching the [:from]
    $timeout(()->
      unwatch()
    , 10000)
]

.directive "footer", ->
  restrict: "A"
  replace: true
  templateUrl: '/views/directives/footer.html'
