'use strict'
###*
 # @ngdoc function
 # @name lunchboxApp.controller:UserCtrl
 # @description
 # # UserCtrl
 # Controller of the lunchboxApp
###

angular.module('lunchboxApp')
.controller 'UserCtrl', [
  '$auth'
  '$scope'
  '$state'
  'currentUser'
  ($auth, $scope, $state, currentUser) ->
    $scope.auth = $auth
    $scope.$watch 'auth.isAuthenticated()', (newVal, oldVal) ->
      $state.go("root") if !newVal
    $scope.currentUser = currentUser
    # console.log $scope.currentUser.get().$resolve()
    $scope.editPhone = false
    $scope.editUsername = false
    $scope.switchPhone = () ->
      $scope.editPhone = !$scope.editPhone
    $scope.switchUsername = () ->
      $scope.editUsername = !$scope.editUsername

    $scope.savePhone = () ->
      $scope.currentUser.get().$save(['profile.phone']).$then () ->
        $scope.editPhone = false
    $scope.saveUsername = (model) ->
      $scope.currentUser.get().$save(['profile.username']).$then () ->
        $scope.editUsername = false
    $scope.stores = $scope.currentUser.get().stores.$resolve()
    return

]

.controller 'UserNewStoreCtrl', [
  '$auth'
  '$scope'
  '$state'
  'currentUser'
  'Store'
  ($auth, $scope, $state, currentUser, Store) ->
    $scope.store = Store.$new()

]

.controller 'UserStoresCtrl', [
  '$auth'
  '$scope'
  '$state'
  'currentUser'
  'Store'
  '$http'
  'settings'
  '$filter'
  ($auth, $scope, $state, currentUser, Store, $http, settings, $filter) ->
    # $scope.stores = $scope.currentUser.get().stores.$resolve()
]
