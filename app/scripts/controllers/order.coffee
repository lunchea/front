'use strict'
###*
 # @ngdoc function
 # @name lunchboxApp.controller:mealsCtrl
 # @description
 # # mealsCtrl
 # Controller of the lunchboxApp
###



angular.module('lunchboxApp')
  .filter "titlecase", ->
    (s) ->
      s = s || ""
      s.toString().toLowerCase().replace /\b([a-z])/g, (ch) ->
        ch.toUpperCase()

  .filter "isConfirmed", ->
    (inputs, opt) ->
      _.filter inputs, (input) ->
        !!input.confirmed_at == opt

  .filter 'reverse', ()->
    return (items)->
      return items.slice().reverse()
  .filter 'storeInfo', ()->
    (store_id, stores)->
      store_map_img = {
        "54b325987363693e92000000": "/images/schomburg.png"
        "54af4561736369593c000000": "/images/engineering.jpg"
        "54e01b85736369225eb10000": "/images/engineering.jpg"
      }
      for store in stores
        if store.id == store_id
          store.map_img = store_map_img[store_id]
          return store

  .controller 'LunchboxCtrl', [
    '$scope', '$timeout', "Order", "OrdersManager", "Menu", "Store"
    "settings", "MockTest", "Alert", "$auth", "$state", "$stateParams",
    "$rootScope", "$http", "Delivery",
    ($scope, $timeout,  Order, OrdersManager, Menu, Store,
     settings, MockTest, Alert, $auth, $state, $stateParams,
     $rootScope, $http, Delivery) ->

      # ===================================
      # ======== Handler functions ========
      # ===================================

      ## Typically put into $timeout, and trigger error using [trigger]
      errorMsgHandler = ( trigger, error_type ) ->
        console.log "Running errorMsgHandler."
        error_msgs = {
          connection: "Connection failed. Please try again later."
          total_cost: "Please select a lunch from the menu."
          empty_order: '''
            Please check: you might have empty orders OR uncustomized
            Sushi (see step 2.5)
            '''
          late_order: '''
            According to restaurant policy,
            we do not accept late orders.
            Please remove or modify corresponding order. Thank you.
            '''
          exceeded: '''
            Please use a @stonybrook.edu address for orders over $60.
            '''
        }
        trigger = if trigger? then trigger else true
        if trigger
          Alert.showError(error_msgs[error_type])
        if error_type == 'late_order' or error_type == 'empty_order'
          $menu = $('.order-menu')
          new_top = $menu.offset().top
          $("html, body").animate scrollTop: new_top
        if $scope.order.in_submission && error_type == 'connection'
          $scope.order.in_submission = false


      # Use to display messages in md-toast
      msgHandler = (msg_type) ->
        msgs = {
          Downloading: "Downloading the menu ..."
          late: '''
            10pm has passed, next-day delivery will not be available.
            We apologize for any inconvenience.
            '''
          submitting: 'Submitting your orders to the server ...'
        }

        # $rootScope.alert.msg = msgs[msg_type]
        Alert.showMsg(msgs[msg_type])


      pickDateSuccessHandler = (index) ->
        # Auto-scroll to policy section
        new_top = 0
        $('date-picker').each (index) ->
          if $(this).offset().top > 0
            new_top = $(this).offset().top
        $("html, body").animate scrollTop: new_top
        return true

      submitSuccessHandler = (data) ->
        ### ==========================================================
          # Retain info  summary display; called after successfully
          # submitting the order.
          # [data] object should be of the form
          #   {"email": "...", "id": "...", "access_token": "..."}
        #========================================================== ###
        $scope.email = data.email
        $scope.order_id = data.id
        $scope.access_token = data.access_token
        $scope.price = data.price
        $state.go "order.show", {id: data.id}

      # ============================================
      # ====== Functions accessible in $scope ======
      # ============================================

      $scope.switchToOrder = (index) ->
        $scope.layout.display_order = index


      $scope.todayDate = () ->
        new Date()

      ## When clicking a date, set $scope.delivered_at to specified date
      $scope.pickDate = (date, delivery) ->
        console.log "date", date
        $scope.layout.height_empty_menu = $('.order-items-wrapper').height()
        delivery.pickDate date, pickDateSuccessHandler

      $scope.showContact = () ->
        order = $scope.order
        if !order.checks(errorMsgHandler)
          return
        # Check if total cost is positive
        if order.totalCost() > 0
          $scope.layout.contact_is_show = true
          ## Remove empty orders; after removal,
          ## manually set to display order #1,
          ## otherwise it may display empty.
          order.removeEmpty()
          $scope.layout.display_order = 0
          auto_scroll = () ->
            # =========================================
            # ======== Auto-scroll preparation ========
            # Auto scroll to contact form section
            # and auto focus
            $order_wrapper = $(".order-items-wrapper")
            y = $order_wrapper.offset().top
            new_top = y + $order_wrapper.height()
            $("html, body").animate scrollTop: new_top
            $("input[name='email']").focus()
            # ======== Auto-scroll preparation end ========
            # =============================================
          $timeout auto_scroll, 200
          return true
        else if order.totalCost() == 0
          errorMsgHandler(true, 'total_cost')
          return true

      $scope.goBack = () ->
        $("html, body").animate scrollTop: 0

      $scope.placeOrder = () ->
        $scope.layout.height_contact = $(".customer-info-active").height()
        order = $scope.order
        # Perform basic checks
        if !order.checks(errorMsgHandler)
          return
        # Now all basic checks are passed, check user-input
        if $scope.customerInfoForm.$valid
          # Submit the order
          if $rootScope.from
            order.source = $rootScope.from
          console.log "order.source = ", order.source

          order.$on 'before-save': (_req) ->
            msgHandler("submitting")
          order.$on 'after-save', (response) ->
            data =
              email: response.email
              id: response.id
              access_token: response.headers("x-order-access-token")

            #| ========================================
            #| submitSuccessHandler() is defined in the
            #| [LunchboxCtrl, controller]
            submitSuccessHandler(response.data)
            #| ========================================
          order.$on 'after-save-error': (response) ->
            errorMsgHandler(!@deliveries[0].id, 'connection')
          order.$save()
        else if !$scope.customerInfoForm.$valid
          $scope.layout.input_is_error = true
          $timeout((->
            $scope.layout.input_is_error = false
          ), 3000)

      # ====== Initialization ======
      ## Some variables to control show/hide of certain sections
      $scope.layout = {
        input_is_complete: false,
        input_is_error: false,
        contact_is_show: false,
        order_success: false,
        order_placed: false,
        display_order: 0,
        is_show_detail: false,
        height_empty_menu: 0,
        height_contact: 0,
        itemIndex: 0,
        notInCategories: (box, delivery) ->
          categories = delivery.categories
          return true
      }

      # =========================================
      ## Initializing the order with one delivery
      $scope.order = Order.$new()
      $scope.order.addDelivery().$reveal()
      msgHandler("Downloading")   ## display alert msg
      $scope.stores = Store.$search().$then (response)->
        today = new Date()
        for store in response
          # Determine whether a store is providing lunch or dinner
          date = new Date(store.delivered_at)
          hour = date.getHours()
          if hour < 14
            store.type = "Lunch"
          else
            store.type = "Dinner"
          # Kick out expired entries
          for box in store.boxes
            expiration_date = new Date(box.expired_at)
            if expiration_date.getTime() < today.getTime()
              box.expired = true


      # Take in the value of <:from> parameter of url
      # http://localhost:9000/#/orders/new/<from>
      # typically should be name of the external source (iFrame, etc.)
      if $stateParams.external
        $rootScope.$broadcast 'fromOutside', from: $stateParams.external

      # Use mock test to simulate [stores] data
      # $scope.stores = MockTest.stores_mock
  ]

  .controller 'OrderConfirmationCtrl', [
    "$scope", "Order", "Store", "OrdersManager", "Alert",
    "$state", "$stateParams", "settings"
    ( $scope, Order, Store, OrdersManager, Alert, $state,
      $stateParams, settings ) ->
      $scope.notice =
        done_loading: false
        message: "Loading Your Order ...."

      $scope.$on "order-loaded", (event, data) ->
        if data.confirm
          $scope.notice.message = 'Thank you for ordering at Lunchea.'
          $scope.notice.done_loading = true
        else if $scope.order.access_token and !data.confirm
          $scope.confirm()
          ## then go to <confirmation-completed, event>
        else
          $scope.notice.message = 'Your order has yet to be confirmed.'
          $scope.notice.done_loading = true

      $scope.$on 'confirmation-completed', (event, data) ->
        if data.confirmed
          $scope.notice.message = '''
            Your order is confirmed.
            Enjoy your lunch!
            '''
          $scope.order.confirm = true
        else
          $scope.notice.message = '''
            Invalid Access Token, we could not confirm your order.
            '''
        $scope.notice.done_loading = true

      $scope.$on 'error-loading-order', (event) ->
        $scope.notice.message = 'Sorry, your order could not be loaded.'
        $scope.notice.done_loading = true

      $scope.cancel = () ->
        OrdersManager.cancel($scope.order)
        .then (response) ->
          console.log response
          $state.go "root"
          Alert.showMsg("Your order has been successfully canceled.")

      $scope.confirm = () ->
        $scope.notice.message = 'Confirming your order, please wait ...'
        OrdersManager.confirm($scope.order)
        .success (data, status, headers, config) ->
          $scope.$broadcast 'confirmation-completed',
            confirmed: true
        .error (data, status, headers, config) ->
          $scope.$broadcast 'confirmation-completed',
            confirmed: false

      console.log "Initializing success/confirmation page ......"
      console.log '--> token', $stateParams
      stores = Store.$search()
      $scope.stores = {}
      stores.$then ->
        console.log $scope.stores
        $scope.stores = {}
        for store in stores
          $scope.stores[store.id] = store.name


      # Get the order, then try to confirm if not already confirmed
      $scope.order = Order.$find($stateParams.id).$then (response) ->
        if $stateParams.access_token
          $scope.order.access_token = $stateParams.access_token
        $scope.$broadcast "order-loaded",
          confirm: !!$scope.order.confirmed_at
      , (error) ->
        # If find() is not successful
        $scope.$broadcast 'error-loading-order'
  ]


  # Controller for display.html
  .controller 'displayOrderCtrl', [
    "$scope", "$http", "$filter", "settings"
    ($scope, $http, $filter, settings) ->
      today = new Date()
      today.getDate()
      $scope.date = $filter('date')(today, 'yyyy-MM-dd')
      $scope.getOrders = () ->
        url = settings.apiUrl + "/orders.json?delivered_at=" + $scope.date
        $http(
          method: "GET"
          url: url
        ).success (data, status, headers, config) ->
          $scope.orders = data
          for order in $scope.orders
            console.log $scope.isEarly(order)

      $scope.getOrders()

      # $scope.isConfirmed = (order) ->
      #   order.confirmed_at?

      $scope.isEarly = (order) ->
        if (order.confirmed_at != null)
          date = new Date(order.confirmed_at).getTime()
        else
          str = parseInt(order.id.substring(0,8), 16) * 1000
          date = new Date(str).getTime()


        deliverDate = new Date(order.delivered_at)
        _date = new Date  deliverDate.getFullYear(),
                          deliverDate.getMonth(),
                          deliverDate.getDate() + 1,
                          9, 0, 0, 0
        dateLate = _date.getTime()
        (date < dateLate)

      $scope.isLate = (order) ->
        !$scope.isEarly(order)
  ]


  .controller "displayStoreDeliveryCtrl", [
    "$scope", "Delivery",
    ($scope, Delivery)->
      console.log Delivery
      store_id = "54af4561736369593c000000"
      
      $scope.today = new Date()

      $scope.deliveries = Delivery.$search(
        store_id: store_id
      ).$then (response)->
        console.log response
  ]

  # Controller for userorder.html
  .controller 'displayUserOrderCtrl', [
    "$scope", "Order", "$filter", "settings", "Store", "$auth", "Alert", "$http"
    ($scope, Order, $filter, settings, Store, $auth, Alert, $http) ->
      # today = new Date()
      # today.getDate()
      # $scope.date = $filter('date')(today, 'yyyy-MM-dd')
      if (!$auth.isAuthenticated())
        return

      today = new Date()

      $scope.email = $auth.getPayload().email
      $scope.getOrders = () ->
        url = settings.apiUrl + "/orders.json?email=" + $scope.email
        $http(
          method: "GET"
          url: url
        ).success (data, status, headers, config) ->
          $scope.orders = data

      $scope.getOrders()

      $scope.stores = Store.$search().$then ()->
        $scope.stores_info = {}
        for store in $scope.stores
          $scope.stores_info[store.id] = store.name

      $scope.isConfirmed = (order) ->
        order.confirm

      $scope.canCancel = (order) ->
        orderCutoff = new Date(order.delivered_at.toString() + " " + "09:00")
        order.confirm && (orderCutoff.getTime() > today.getTime())

      $scope.canConfirm = (order) ->
        orderCutoff = new Date(order.delivered_at.toString() + " " + "09:00")
        (!(order.confirm)) && (orderCutoff.getTime() > today.getTime())

      $scope.isDelivered = (order) ->
        deliveryDate = new Date(order.delivered_at.toString() + " " + "12:45")
        order.confirm && (deliverDate.getTime() <= today.getTime())

      $scope.confirmed_toBeDelivered = (order) ->
        deliveryDate = new Date(order.delivered_at.toString() + " " + "12:45")
        orderCutoff = new Date(order.delivered_at.toString() + " " + "09:00")
        order.confirm && (orderCutoff.getTime() < today.getTime()) &&
        (today.getTime() <= deliverDate.getTime())

      $scope.expired = (order) ->
        orderCutoff = new Date(order.delivered_at.toString() + " " + "09:00")
        console.log orderCutoff.toDateString()
        (!(order.confirm)) && (orderCutoff.getTime() <= today.getTime())

      $scope.cancel = (order) ->
        OrdersManager.cancel(order)
        .success (data, status, headers, config) ->
          Alert.showMsg("Your order has been successfully canceled.")

      $scope.confirm = (order) ->
        $scope.notice.message = 'Confirming your order, please wait ...'
        OrdersManager.confirm(order)
        .success (data, status, headers, config) ->
          $scope.$broadcast 'confirmation-completed',
            confirmed: true
        .error (data, status, headers, config) ->
          $scope.$broadcast 'confirmation-completed',
            confirmed: false

      $scope.$on 'confirmation-completed', (event, data) ->
        if data.confirmed
          $scope.notice.message = '''
            Your order is confirmed.
            Enjoy your lunch!
            '''
          $scope.order.confirm = true
        else
          $scope.notice.message = '''
            Invalid Access Token, we could not confirm your order.
            '''
        # $scope.notice.done_loading = true


      # $scope.isEarly = (order) ->
      #   if (order.confirmed_at != null)
      #     date = new Date(order.confirmed_at.toString()).getTime()
      #   else
      #     str = parseInt(order.id.toString().substring(0,8), 16) * 1000
      #     date = new Date(str).getTime()

      #   # console.log new Date(date)
      #   deliverDate = new Date(order.delivered_at.toString())
      #   _date = new Date  deliverDate.getFullYear(),
      #                     deliverDate.getMonth(),
      #                     deliverDate.getDate() + 1,
      #                     9, 0, 0, 0
      #   dateLate = _date.getTime()
      #   (date < dateLate)

      # $scope.isLate = (order) ->
      #   !$scope.isEarly(order)
  ]
