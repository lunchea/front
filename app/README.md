# 0. `ng-app`: `lunchboxApp`
- Defined and configured in `scripts/app.coffee`

## Parent controller: `BlockCtrl`
- Control the display of major sections.

### Controller `LunchboxCtrl`
- Controls the order-page section
- Defined in `scripts/controllers/order.coffee`

#### Functions
##### `Orders` `class`
- To organize multiple orders
- Obvious attributes
- - `.all`: a `list` of current existing `Order`s. Each element is an `Order` object from `scripts/decorators/orderdecorator.coffee`.
- - `email`: a `string`, which will hold the customer's email.
- - `phone`: a `string`, which will hold the customer's phone number.
- Methods
- - `.addDelivery()`: add to the end of `.all` a new `Order`
- - `.removeDelivery()`: `if` there are more than one order, remove current `Order` from `.all`.
- - `.totalCost()`: add up the total cost of all existing `Order`. Each `Order` has its built-in `.totalCost()` method.
- - `.placeOrder()`: submit the order to the server. There are several checks: 

- - 1) `if` there are empty `Order` (those with zero `totalCost()`), display alert if so

- - - `if` user-inputs are valid `and if` the <place-order> button was not clicked before, proceed to submit data. 

- - 2) `else if` the user-inputs are `invalid`, alert the user by turning on `$scope.layout.input_is_error`, which will `ng-show` the error messages inside directives `inputEmail` and `inputPhone`.



##### `errorHandler()` and `msgHandler()`

##### `getOrders()`

#### Dependencies
- ##### `Order`

- ##### `OrderItem`

### Directive `order`
- Defined in `scripts/controllers/order.coffee`

### Directive `orderItem`
- Defined in `scripts/controllers/order.coffee`

### Directive `inputEmail`
- Defined in `scripts/controllers/order.coffee`

### Directive `inputPhone`