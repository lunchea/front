/* jshint node:true */
'use strict';
// generated on 2014-12-13 using generator-gulp-webapp 0.2.0
var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var browserSync = require('browser-sync');
var spa         = require("browser-sync-spa");
var reload = browserSync.reload;
var secrets = require('./config/secrets.json');

gulp.task('styles', function () {
  gulp.src('./bower_components/angular-material/angular-material.css')
    .pipe($.rename('./bower_components/angular-material/angular-material.scss'))
    .pipe(gulp.dest("./"));

  return gulp.src('app/styles/*.scss')
    .pipe($.plumber())
    .pipe($.sass({
      outputStyle: 'nested', // libsass doesn't support expanded yet
      precision: 10,
      includePaths: ['.'],
      onError: console.error.bind(console, 'Sass error:')
    }))
    .pipe($.autoprefixer({browsers: ['last 1 version']}))
    .pipe(gulp.dest('.tmp/styles'));
});

gulp.task('styles:mobile', function () {
  return gulp.src('mobile_app/styles/*.scss')
    .pipe($.plumber())
    .pipe($.sass({
      outputStyle: 'nested', // libsass doesn't support expanded yet
      precision: 10,
      includePaths: ['.'],
      onError: console.error.bind(console, 'Sass error:')
    }))
    .pipe($.autoprefixer({browsers: ['last 1 version']}))
    .pipe(gulp.dest('.tmp_mobile/styles'));
});

gulp.task('jshint', function () {
  return gulp.src('app/scripts/**/*.js')
    .pipe(reload({stream: true, once: true}))
    .pipe($.jshint())
    .pipe($.jshint.reporter('jshint-stylish'))
    .pipe($.if(!browserSync.active, $.jshint.reporter('fail')));
});

gulp.task('jshint:mobile', function () {
  return gulp.src('mobile_app/scripts/**/*.js')
    .pipe($.jshint())
    .pipe($.jshint.reporter('jshint-stylish'))
    .pipe($.jshint.reporter('fail'));
});

gulp.task('scripts', function () {
  gulp.src('./components/**/*.coffee')
    .pipe($.coffee())
    .pipe(gulp.dest('.tmp/components/'));

    return gulp.src('app/**/*.coffee')
        .pipe($.coffee())
        .pipe(gulp.dest('.tmp/'));
});

gulp.task('scripts:mobile', function () {
  gulp.src('./components/**/*.coffee')
    .pipe($.coffee())
    .pipe(gulp.dest('.tmp_mobile/components/'));

    return gulp.src('mobile_app/**/*.coffee')
        .pipe($.coffee())
        .pipe(gulp.dest('.tmp_mobile/'));
});

gulp.task('html', ['styles', 'scripts'], function () {
  var assets = $.useref.assets({searchPath: '{.tmp,app}'});

  return gulp.src('app/**/*.html')
    .pipe(assets)
    .pipe($.if('*.js', $.replace('http://localhost:3000', 'https://api.lunchea.com')))
    .pipe($.if('*.js', $.ngAnnotate()))
    .pipe($.if('*.js', $.uglify()))
    .pipe($.if('*.css', $.csso()))
    .pipe($.rev())
    .pipe(assets.restore())
    .pipe($.useref())
    .pipe($.revReplace())
    .pipe($.if('*.html', $.minifyHtml({conditionals: true, loose: true, empty: true})))
    .pipe(gulp.dest('dist'));
});

gulp.task('html:mobile', ['styles:mobile', 'scripts:mobile'], function () {
  var assets = $.useref.assets({searchPath: '{.tmp_mobile,mobile_app}'});

  return gulp.src('mobile_app/**/*.html')
    .pipe(assets)
    .pipe($.if('*.js', $.replace('http://localhost:3000', 'https://api.lunchea.com')))
    .pipe($.if('*.js', $.ngAnnotate()))
    .pipe($.if('*.js', $.uglify()))
    .pipe($.if('*.css', $.csso()))
    .pipe(assets.restore())
    .pipe($.useref())
    .pipe($.if('*.html', $.minifyHtml({conditionals: true, loose: true, empty: true})))
    .pipe(gulp.dest('www'));
});

gulp.task('images', function () {
  return gulp.src('app/images/**/*')
    .pipe($.cache($.imagemin({
      progressive: true,
      interlaced: true
    })))
    .pipe(gulp.dest('dist/images'));
});

gulp.task('images:mobile', function () {
  return gulp.src('mobile_app/images/**/*')
    .pipe($.cache($.imagemin({
      progressive: true,
      interlaced: true
    })))
    .pipe(gulp.dest('www/images'));
});

gulp.task('fonts', function () {
  return gulp.src(require('main-bower-files')().concat('app/fonts/**/*'))
    .pipe($.filter('**/*.{eot,svg,ttf,woff}'))
    .pipe($.flatten())
    .pipe(gulp.dest('dist/fonts'));
});

gulp.task('fonts:mobile', function () {
  return gulp.src(require('main-bower-files')().concat('mobile_app/fonts/**/*'))
    .pipe($.filter('**/*.{eot,svg,ttf,woff}'))
    .pipe($.flatten())
    .pipe(gulp.dest('www/fonts'));
});

gulp.task('extras', function () {
  gulp.src([
    'components/**/*.*',
    '!components/**/*.coffee',
    '!components/**/*.js'
  ]).pipe(gulp.dest('dist/components'));
  return gulp.src([
    'app/*.*',
    '!app/*.html',
    'node_modules/apache-server-configs/dist/.htaccess'
  ], {
    dot: true
  }).pipe(gulp.dest('dist'));
});

gulp.task('extras:mobile', function () {
  gulp.src([
    'components/**/*.*',
    '!components/**/*.coffee',
    '!components/**/*.js'
  ]).pipe(gulp.dest('www/components'));
  return gulp.src([
    'mobile_app/*.*',
    '!mobile_app/*.html',
    'node_modules/apache-server-configs/dist/.htaccess'
  ], {
    dot: true
  }).pipe(gulp.dest('www'));
});

gulp.task('clean', require('del').bind(null, ['.tmp']));

gulp.task('connect:mobile', ['styles:mobile', 'scripts:mobile'], function () {
  var serveStatic = require('serve-static');
  var serveIndex = require('serve-index');
  var app = require('connect')()
    .use(require('connect-livereload')({port: 35729}))
    .use(serveStatic('.tmp_mobile'))
    .use(serveStatic('mobile_app'))
    // paths to bower_components should be relative to the current file
    // e.g. in app/index.html you should use ../bower_components
    .use('/bower_components', serveStatic('bower_components'))
    .use('/components', serveStatic('components'))
    .use(serveIndex('mobile_app'));

  require('http').createServer(app)
    .listen(9000)
    .on('listening', function () {
      console.log('Started connect web server on http://localhost:9000');
    });
});

gulp.task('serve', ['styles', 'scripts', 'fonts'], function () {
  browserSync.use(spa({
      selector: "[ng-app]" // Only needed for angular apps
  }));

  browserSync({
    notify: false,
    port: 9000,
    server: {
      baseDir: ['.tmp', 'app'],
      routes: {
        '/bower_components': 'bower_components',
        '/components': 'components'
      }
    }
  });

  // watch for changes
  gulp.watch([
    'app/*.html',
    '{.tmp,app}/scripts/**/*.js',
    '.tmp/styles/**/*.css',
    '.tmp/fonts/**/*',
    'components/**/*.html',
    'app/images/**/*'
  ]).on('change', reload);

  gulp.watch('app/styles/**/*.scss', ['styles']);
  gulp.watch('app/scripts/**/*.coffee', ['scripts']);
  gulp.watch('app/fonts/**/*', ['fonts']);
  gulp.watch('components/**/*.coffee', ['scripts']);
  gulp.watch('bower.json', ['wiredep', 'fonts']);
});

gulp.task('serve:mobile', ['connect:mobile', 'watch:mobile'], function () {
  require('opn')('http://localhost:9000');
});

// inject bower components
gulp.task('wiredep', function () {
  var wiredep = require('wiredep').stream;

  gulp.src('app/styles/*.scss')
    .pipe(wiredep())
    .pipe(gulp.dest('app/styles'));

  gulp.src('app/*.html')
    .pipe(wiredep())
    .pipe(gulp.dest('app'));
});

gulp.task('watch:mobile', ['connect:mobile'], function () {
  $.livereload.listen();

  // watch for changes
  gulp.watch([
    'mobile_app/*.html',
    '.tmp_mobile/styles/**/*.css',
    '{.tmp_mobile,mobile_app}/scripts/**/*.js',
    'components/**/*.html',
    'components/**/*.js',
    'mobile_app/images/**/*'
  ]).on('change', $.livereload.changed);

  gulp.watch('mobile_app/styles/**/*.scss', ['styles:mobile']);
  gulp.watch('mobile_app/scripts/**/*.coffee', ['scripts:mobile']);
  gulp.watch('components/**/*.coffee', ['scripts:mobile']);
});

gulp.task('build', ['jshint', 'html', 'images', 'fonts', 'extras'], function () {
  return gulp.src('dist/**/*').pipe($.size({title: 'build', gzip: true}));
});

gulp.task('build:mobile', ['html:mobile', 'images:mobile', 'fonts:mobile', 'extras:mobile'], function () {
  return gulp.src('www/**/*').pipe($.size({title: 'build', gzip: true}));
});

gulp.task('default', ['clean'], function () {
  gulp.start('build');
});
