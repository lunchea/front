'use strict'

###*
 # @ngdoc overview
 # @name starter
 # @description
 # # starter
 #
 # Main module of the application.
###
angular
  .module 'starter'
  .config [
    '$stateProvider'
    '$urlRouterProvider'
    ($stateProvider, $urlRouterProvider) ->
      # Ionic uses AngularUI Router which uses the concept of states
      # Learn more here: https://github.com/angular-ui/ui-router
      # Set up the various states which the app can be in.
      # Each state's controller can be found in controllers.js

      # setup an abstract state for the tabs directive

      # Each tab has its own nav history stack:
      $urlRouterProvider.otherwise "/tab/store"
      $stateProvider
        .state "tab",
          url: "/tab"
          abstract: true
          templateUrl: "templates/tabs.html"
          controller: "OrderCtrl"
        .state "tab.order",
          url: "/order"
          views:
            "menuContent":
              templateUrl: "templates/tab-order.html"
              controller: "DashCtrl"

        .state "tab.store",
          url: "/store"
          views:
            "menuContent":
              templateUrl: "templates/tab-store.html"
              controller: "DashCtrl"

        .state "tab.date",
          url: "/order/date"
          views:
            "menuContent":
              templateUrl: "templates/tab-date.html"
              controller: "BoxCtrl"

        .state "tab.box",
          url: "/order/boxes"
          views:
            "menuContent":
              templateUrl: "templates/tab-box.html"
              controller: "BoxCtrl"

        .state "tab.item",
          url: "/order/item"
          views:
            "menuContent":
              templateUrl: "templates/tab-item.html"

        .state "tab.account",
          url: "/account"
          views:
            "menuContent":
              templateUrl: "templates/tab-account.html"
              controller: "AccountCtrl"

        .state "register",
          url: "/register"
          templateUrl: "templates/register.html"
          controller: "AccountCtrl"

        .state "tab.history",
          url: "/history"
          views:
            "menuContent":
              templateUrl: "templates/tab-history.html"
              controller: "OrderHistoryCtrl"


  ]