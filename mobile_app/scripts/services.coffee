'use strict'

###*
 # @ngdoc function
 # @name starter.services
 # @description
 # services of the starter
###
angular
.module 'starter.services', [
  'lunchboxApp.settings'
  'lunchboxApp.services'
]

