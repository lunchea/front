'use strict'

###*
 # @ngdoc function
 # @name starter.controllers
 # @description
 # controllers of the starter
###
angular
.module 'starter.controllers', [
  'starter.services'
]

.controller "DashCtrl", ['$scope', 'Stores'
  ($scope, Stores) ->
    $scope.check = () ->
      console.log '$scope', $scope.delivery
    # $scope.stores = Stores.get
]

.controller "OrderCtrl", [
  '$scope', 'Stores', '$stateParams', 'Order', '$localStorage'
  '$state', 'currentUser', '$ionicModal', '$auth', 'User'
  ( $scope, Stores, $stateParams, Order, $localStorage, $state,
    currentUser, $ionicModal, $auth, User ) ->
    $scope.credentials = {}
    $scope.$auth = $auth
    $scope.order = Order.$new()
    fillUser = () ->
      $scope.order.email = currentUser().email
      $scope.order.phone = currentUser().profile.phone

    if currentUser().email
      fillUser()
    $scope.$on 'login', () ->
      fillUser()

    $scope.order.$on 'after-save', () ->
      $scope.order = {}
    $scope.order.$on 'after-save-error', () ->
      $scope.saveError = true

    $scope.submitOrder = () ->
      $scope.order.$save().$then () ->
        console.log 'si', @

    $scope.stores = Stores.get
    $scope.newDelivery = () ->
      $scope.delivery = $scope.order.addDelivery()
    $scope.newDelivery()
    $scope.setDelivery = (delivery) ->
      $scope.delivery = delivery
    $scope.setItem = (item) ->
      $scope.item = item
    $scope.newItem = (item) ->
      item.$reveal true
      $state.go 'tab.box'
    $scope.choose = (box) ->
      $scope.item = $scope.delivery.addEmptyItem()
      $scope.item.getBox box
      console.log '$scope.item', $scope.delivery

    $ionicModal.fromTemplateUrl '/templates/login.html',
      scope: $scope
    .then (modal) ->
      $scope.modal = modal

    # Open the login modal
    $scope.login = () ->
      $scope.modal.show()
    $scope.closeLogin = () ->
      $scope.modal.hide()
    $scope.$on '$destroy', () ->
      $scope.modal.remove()

    $scope.signIn = ->
      # $scope.modal.hide()
      $auth.login($scope.credentials)
      .then (response) ->
        $localStorage.currentUser = User.$buildRaw(response.data)
        $scope.modal.hide()
        $scope.$emit 'login'
      .catch (error) ->
        $scope.credentials.errors = {auth: ['invalid email or password']}
        console.log $scope.credentials
]

.controller "DeliveryCtrl", [
  '$scope', 'Stores', '$stateParams'
  ($scope, Stores, $stateParams) ->
    Stores.get.$then () ->
      $scope.store = _.find Stores.get, {id: $stateParams.storeId}
      $scope.delivery.pickStore $scope.store
      console.log 'dsf', $scope.delivery.categories
]

.controller "BoxCtrl", [
  '$scope', '$state'
  ($scope, $state) ->
]

.controller "AccountCtrl", [
  '$scope', 'User', '$auth', '$localStorage', 'currentUser', '$state'
  ($scope, User, $auth, $localStorage, currentUser, $state) ->
    $scope.logedin
    if $auth.isAuthenticated()
      $scope.settings = enableFriends: true
      $scope.logedin = true
    else
      $scope.logedin = false
    $scope.logout = ->
      $auth.logout()
      $state.go 'tab.store'
    $scope.isAuthenticated = ->
      $auth.isAuthenticated()
    $scope.currentUser = currentUser
    console.log 'us', currentUser()

]

.controller "OrderHistoryCtrl", [
  '$scope', 'Order', '$auth', '$localStorage', 'currentUser', '$state'
  ($scope, Order, $auth, $localStorage, currentUser, $state) ->
    $scope.unAuthed = true if !$auth.isAuthenticated()
    $scope.orders = Order.$search
      user_id: currentUser().id
]

